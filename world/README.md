# World
A sample 555 timer LED blinky project to demonstrate Surface Mount KiCAD
design and pipeline automation.

To view all project outputs of the
[master branch](https://ci-includes.gitlab.io/kici/world/navigate/world-navigate.html)
or the
[interactive bill of materials](https://ci-includes.gitlab.io/kici/world/ibom/world-ibom.html).

For release files, see the
[releases page](https://gitlab.com/ci-includes/kici/-/releases) or the
[package repository](https://gitlab.com/ci-includes/kici/-/packages).

![PCB](https://ci-includes.gitlab.io/kici/world/png/hello-3D_blender_front.png)
