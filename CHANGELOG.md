# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v0.4.0] - 2024-02-14
### Fixed
- Add missing top-zoom level
- Support deploy on singular projects

### Removed
- Remove paste and adhesion from fab layers

### Added
- Publish to gitlab pages
- Include more pages views


## [v0.3.0] - 2024-02-02
### Fixed
- Move to KiBot version 1.6.4 from dev
- PDF Centering
- Run attach diff only for MR's


## [v0.2.0] - 2024-01-31
### Added
- Support release generation


## [v0.1.0] - 2024-01-24
### Added
- KiCi catalog component
